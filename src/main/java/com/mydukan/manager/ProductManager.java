package com.mydukan.manager;

import com.mydukan.dao.MongoClientFactory;
import com.mydukan.pojo.BasicResponce;
import com.mydukan.pojo.Product;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;


@Service
public class ProductManager {
    MongoOperations mongoOperations = (MongoOperations) new MongoTemplate(MongoClientFactory.INSTANCE.getClient(),"mydukan");

    public void saveProduct(Product product, BasicResponce responce) {
        try {
            mongoOperations.save(product);
            responce.setStatus(200);
            responce.setMessage("success");
        }catch (Exception e){
            responce.setMessage("Bad Request");
            responce.setStatus(400);
        }
    }

    public List<Product> getProducts(List<Product> list) {
        list= mongoOperations.findAll(Product.class);
        return list;
    }
}
