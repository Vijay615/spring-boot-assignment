package com.mydukan.controller;

import com.mydukan.dao.MongoClientFactory;
import com.mydukan.manager.ProductManager;
import com.mydukan.pojo.BasicResponce;
import com.mydukan.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@RestController
public class ProductController {
    @Autowired
    ProductManager manager;

    // add product in vijay features

    @RequestMapping(value = "/product/add", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public BasicResponce adminSignUp(@RequestBody Product product) {
        BasicResponce responce = new BasicResponce();
        manager.saveProduct(product,responce);
        return responce;

    }
    @RequestMapping(value = "/products/get", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
    public List<Product> getAllProduct(){
        List<Product> list=new ArrayList<Product>();
        list =manager.getProducts(list);
        return list;
    }
}
