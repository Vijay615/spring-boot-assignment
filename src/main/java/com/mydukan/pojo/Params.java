package com.mydukan.pojo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Params {
    String details;
    String  notes;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
