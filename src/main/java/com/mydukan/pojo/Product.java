package com.mydukan.pojo;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Product {
    String name;
    String price;
    Params params;

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public void setName(String name) {
        this.name = name;
    }
}
